const { url, param } = require('./task');
const Post = require('./models/post.js');
const mongoose = require('mongoose');

describe('database', () => {
    beforeAll(async () => {
        await mongoose.connect(url, param);
    });

    afterAll(async () => {
        await mongoose.disconnect(url, param);
    });
    test('Проверка данных (размер): ', async () => {
        const data = await Post.find();
        expect(data).toHaveLength(3);
    });

    test('Проверка данных (наличие свойств объекта): ', async () => {
        const data = await Post.find();
        expect(data[0]).toHaveProperty('genre');
        expect(data[0]).toHaveProperty('name');
        expect(data[0]).toHaveProperty('description');
    });

    test('Проверка данных (объект не содержит нежелательного свойства): ', async () => {
        const data = await Post.find();
        expect(data[0]).not.toHaveProperty('rating');
    });
});
